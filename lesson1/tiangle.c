#include <stdlib.h>
#include <stdio.h>
#include <math.h>


int main()
{
    int a, b, c;
    printf("input 3 len:");
    int res_scanf = 0;
    res_scanf = scanf("%d %d %d", &a, &b, &c);
    if (res_scanf != 3)
    {
        puts("Error\n");
        return -1;
    }

    if (a + b < c)
    {
        printf("impossible \n");
    }
    else if (a*a + b*b == c*c)
    {
        printf("rectangular \n");
    }
    else if(a*a + b*b > c*c)
    {
        printf("acute \n");
    }
    else 
    {
        printf("obtuse \n");
    }


    return 0;
}

